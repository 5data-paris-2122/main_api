const { Schema, model } = require("mongoose");

const studentsSchema = new Schema({
  supId: {
    type: String,
    required: true,
    unique: true,
  },
  name: String,
  lastname: String,
  email: {
    type: String,
    unique: true,
  },
  birthdate: String,
  address: {
    street: String,
    city: String,
    country: String,
  },
  education: [
    {
      schoolname: String,
      degrees: String,
    },
  ],
  internships: [
    {
      company_name: String,
      year: Number,
    },
  ],
  sup_cursus: [
    {
      class_name: String,
      campus: String,
      attendance: Number,
      attended_open_day: Boolean,
      contract: {
        job_title: String,
        company_name: String,
      },
      modules: [
        {
          name: String,
          grades: Number,
        },
      ],
    },
  ],
  post_cursus: {
    future_job: {
      job_title: String,
      company_name: String,
    },
    quit_reason: String,
    new_school: String,
  },
});

module.exports = model("students", studentsSchema);
