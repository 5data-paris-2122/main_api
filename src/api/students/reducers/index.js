//Validator
const isEmpty = (string) => string && string.trim() === "";

exports.validateData = (data) => {
  let errors = {};
  if (isEmpty(data.supId)) errors.supId = "Must not be empty";

  return {
    errors,
    valid: Object.keys(errors).length === 0,
  };
};

exports.reduceData = (data) => {
  let content = {};
  if (!isEmpty(data.supId)) {
    content.supId = data.supId;
    content.email = `${data.supId}@supinfo.com`;
  }
  if (Object.keys(data).includes("name") && !isEmpty(data.name)) {
    content.name = data.name;
  }
  if (Object.keys(data).includes("lastname") && !isEmpty(data.lastname)) {
    content.lastname = data.lastname;
  }
  if (Object.keys(data).includes("birthdate") && !isEmpty(data.birthdate)) {
    content.birthdate = data.birthdate;
  }
  if (Object.keys(data).includes("address")) {
    const address = data.address;
    content.address = {};
    if (Object.keys(address).includes("street") && !isEmpty(address.street)) {
      content.address.street = address.street;
    }
    if (Object.keys(address).includes("city") && !isEmpty(address.city)) {
      content.address.city = address.city;
    }
    if (Object.keys(address).includes("country") && !isEmpty(address.country)) {
      content.address.country = address.country;
    }
  }
  if (Object.keys(data).includes("education")) {
    const education = data.education;
    content.education = [];
    for (let i = 0; i < education.length; i++) {
      const edu = education[i];
      content.education[i] = {};
      if (Object.keys(edu).includes("schoolname") && !isEmpty(edu.schoolname)) {
        content.education[i].schoolname = edu.schoolname;
      }
      if (Object.keys(edu).includes("degrees") && !isEmpty(edu.degrees)) {
        content.education[i].degrees = edu.degrees;
      }
    }
  }
  if (Object.keys(data).includes("internships")) {
    const internships = data.internships;
    content.internships = [];
    for (let i = 0; i < internships.length; i++) {
      const intern = internships[i];
      content.internships[i] = {};
      if (Object.keys(intern).includes("company_name") && !isEmpty(intern.company_name)) {
        content.internships[i].company_name = intern.company_name;
      }
      if (Object.keys(intern).includes("year") && !isNaN(intern.year)) {
        content.internships[i].year = intern.year;
      }
    }
  }
  if (Object.keys(data).includes("sup_cursus")) {
    const sup_cursus = data.sup_cursus;
    content.sup_cursus = [];
    for (let i = 0; i < sup_cursus.length; i++) {
      const supcurs = sup_cursus[i];
      content.sup_cursus[i] = {};
      if (Object.keys(supcurs).includes("class_name") && !isEmpty(supcurs.class_name)) {
        content.sup_cursus[i].class_name = supcurs.class_name;
      }
      if (Object.keys(supcurs).includes("campus") && !isEmpty(supcurs.campus)) {
        content.sup_cursus[i].campus = supcurs.campus;
      }
      if (Object.keys(supcurs).includes("attendance") && !isNaN(supcurs.attendance)) {
        content.sup_cursus[i].attendance = supcurs.attendance;
      }
      if (Object.keys(supcurs).includes("attended_open_day") && typeof supcurs.attended_open_day === "boolean") {
        content.sup_cursus[i].attended_open_day = supcurs.attended_open_day;
      }
      if (Object.keys(supcurs).includes("contract")) {
        const contract = supcurs.contract;
        content.sup_cursus[i].contract = {};
        if (Object.keys(contract).includes("job_title") && !isEmpty(contract.job_title)) {
          content.sup_cursus[i].contract.job_title = contract.job_title;
        }
        if (Object.keys(contract).includes("company_name") && !isEmpty(contract.company_name)) {
          content.sup_cursus[i].contract.company_name = contract.company_name;
        }
      }
      if (Object.keys(supcurs).includes("modules")) {
        const modules = supcurs.modules;
        content.sup_cursus[i].modules = [];
        for (let j = 0; j < modules.length; j++) {
          const module = modules[j];
          content.sup_cursus[i].modules[j] = {};
          if (Object.keys(module).includes("name") && !isEmpty(module.name)) {
            content.sup_cursus[i].modules[j].name = module.name;
          }
          if (Object.keys(module).includes("grades")) {
            content.sup_cursus[i].modules[j].grades = module.grades;
          }
        }
      }
    }
  }
  if (Object.keys(data).includes("post_cursus")) {
    const post_cursus = data.post_cursus;
    content.post_cursus = {};
    if (Object.keys(post_cursus).includes("future_job")) {
      if (post_cursus.future_job !== "") {
        const future_job = post_cursus.future_job;
        content.post_cursus.future_job = {};
        if (Object.keys(future_job).includes("job_title") && !isEmpty(future_job.job_title)) {
          content.post_cursus.future_job.job_title = future_job.job_title;
        }
        if (Object.keys(future_job).includes("company_name") && !isEmpty(future_job.company_name)) {
          content.post_cursus.future_job.company_name = future_job.company_name;
        }
      }
    }
    if (Object.keys(post_cursus).includes("quit_reason") && !isEmpty(post_cursus.quit_reason)) {
      content.post_cursus.quit_reason = post_cursus.quit_reason;
    }
    if (Object.keys(post_cursus).includes("new_school") && !isEmpty(post_cursus.new_school)) {
      content.post_cursus.new_school = post_cursus.new_school;
    }
  }

  return content;
};
