const app = require("express")();
const students = require("./students/routes");

app.use("/students", students);

module.exports = app;
